import matplotlib.pyplot as plt
import matplotlib
matplotlib.style.use('ggplot')


def graph(list_1,list_2,NAME):
    layer_1 = 0
    layer_2 = 0
    e = 0
    M = list_1
    while e == 0:
        try:
            M = M[0]
            layer_1 += 1  
            if M == M[0]:
                e = 1
          
        except:
            e = 1
    e = 0
    M = list_2
    while e == 0:
        try:
            M = M[0]
            layer_2 += 1
            if M == M[0]:
                layer_2 -= 1
                e = 1
        except:
            e = 1 
    if NAME == None:
        if layer_2 != 0:
            NAME = [None] * len(list_2)
    if layer_1 > 1 or layer_2 > 1:
            
            for i in range(len(list_2)):
                if len(list_2) != len(list_1):
                    if str(NAME) == NAME:
                        simple_graph(list_1,list_2[i],NAME)
                    else:
                        simple_graph(list_1,list_2[i],NAME[i])
                    
                else:
                    if str(NAME) == NAME:
                        simple_graph(list_1[i],list_2[i],NAME)
                    else:            
                        simple_graph(list_1[i],list_2[i],NAME[i])
                
    else: 
         
        simple_graph(list_1,list_2,NAME)
    
    plt.grid()
    plt.show()
def simple_graph(list_1,list_2,NAME):
    try:
        if list_1 != None and str(list_1) == list_1:
            N = []
            for i in range(len(list_1)):
                N.append(i)
            plt.xticks(N, list_1)
            list_1 = 0
    except:
        pass
    try:
        if list_2 != None  and str(list_2) == list_2:
            N = []
            for i in range(len(list_2)):
                N.append(i)
            plt.xticks(N, list_2)
            list_2 = 0
    except:
        pass    
    if NAME != [] and NAME != None:
        if list_1 != None and list_2 != None:
            plt.plot(list_1,list_2,label=NAME)
        elif list_2 != None:
            plt.plot(list_2,label=NAME)
        else:
            plt.plot(list_1,label=NAME) 
        plt.legend(bbox_to_anchor=(0.7, 1.14), loc=2, borderaxespad=0)
    else:
        if list_1 != None and list_2 != None:
            plt.plot(list_1,list_2)
        elif list_2 != None:
            plt.plot(list_2)
        else:
            plt.plot(list_1)         
   
"""graph([1,2,3,4],[[1,2,3,4],[4,3,2,1]],None)
graph([1,2,3,4],[52,42,62,82],None)
graph(None,[52,42,62,82],None)
graph([1,2,3,4],None,None)
graph([[1,2,3,10],[1,2,3,5]],[[1,2,3,4],[4,3,2,1]],["notes en prepa","notes en ING1"])
graph(["chat","chien","taupe","poisson"],[5,25,2,1],"poid")
graph([5,25,2,1],["chat","chien","taupe","poisson"],"poid")
graph(["chat","chien"],[[5,25],[2,1]],["poid","taille"])
graph(None,[[1,2,3,4],[4,3,2,1],[4,8,12,52,1]],None)"""