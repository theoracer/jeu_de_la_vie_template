
class map():
        def __init__(self,canvas,x):
                x += 1
                self.ratio = 700 / x
                r = self.ratio/2
                maxi = self.ratio * x
                for i in range(x):
                        x = i * self.ratio
                        canvas.create_line(x+r,maxi-r,x+r,0+r,fill="black",width = 2)
                        canvas.create_line(maxi-r,x+r,0+r,x+r,fill="black",width = 2)
        def get_ratio(self):
                return self.ratio
class cursor():
        def __init__(self,canvas,pos):
                x = pos[0]
                y = pos[1]
                self.pos = pos
                self.place = ( x -45 , y - 5 , x - 35 , y + 5 ) 
                self.speed = 80
                canvas.create_line(x-45,y,x+45,y,fill="black",width = 2)
                canvas.create_line(x-45,y-15,x-45,y+15,fill="black",width = 2)
                canvas.create_line(x+45,y-15,x+45,y+15,fill="black",width = 2)
                self.text = canvas.create_text(self.pos[0],self.pos[1]-30,text=0, font="30", fill="magenta2")
                self.head = canvas.create_rectangle(self.place,fill="black",width = 0)
        def act (self,canvas,X,Y):
                if abs( X - self.pos[0] ) < 45 and abs( Y - self.pos[1] ) < 15:
                        if X < self.pos[0] - 40:
                                x1 = self.pos[0] - 45
                                x2 = self.pos[0] - 35
                                self.speed = 80
                        elif X > self.pos[0] + 40:
                                x1 = self.pos[0] + 45
                                x2 = self.pos[0] + 35 
                                self.speed = 0
                        else:
                                x1 = X - 5
                                x2 = X + 5
                                self.speed = (self.pos[0] + 40 - X)
                        canvas.delete(self.text)
                        self.text = canvas.create_text(self.pos[0],self.pos[1]-30,text=80-self.speed, font="30", fill="magenta2")
                        self.place = (x1,self.place[1],x2,self.place[3])
                        canvas.coords(self.head,self.place)
                        
                        return self.speed
                
                else:
                        return self.speed
                
class button():
        def __init__(self,canvas,pos,name,color):
                self.pos = pos[0] , pos[1]
                canvas.create_rectangle(self.pos[0] - 40,self.pos[1] - 10,self.pos[0] + 40,self.pos[1] + 10,fill=color,width = 0)
                canvas.create_text(self.pos[0],self.pos[1],text=name, font="30", fill="red")
                   
        def act (self,X,Y):
                if abs( X - self.pos[0] ) < 40 and abs( Y - self.pos[1] ) < 10:
                        return 1
                else:
                        return 0
                
                

                        