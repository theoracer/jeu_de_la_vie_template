from time import clock
from Tkinter import *
from random import *
from math import *
import os
import os.path
from cell import *
from mappy import *
from myfile import *
from graph_h import *
import copy
a = 0
fen=Tk()
fen.focus()
canvas=Canvas(fen, width=800, height=700,bg="white")
current_map = "virgin"
path = "/map/"
def click(event):
    global X,Y
    X = event.x
    Y = event.y

def start(name): 
    global Mat,Map,Cell,X,Y,speed,count
    Mat = []
    Map = 0
    Cell = []
    X = 0
    Y = 0
    count = 0
    speed = 80
    try:
        Mat = myopen(name,path).pop()
    except:
        exit()
    init()
    read_mat()
    fen.after(200,main)
    

def init():
    global Map,Mat,Cursor,Button
    Map = map(canvas,len(Mat))
    Cursor = cursor(canvas,(740,100))
    Button = button(canvas,(740,170),"RESET","black")
    modul()

def modul():
    global canvas,template
    Y = 225
    X = 700
    template = [[],0]
    template[1] = myopen("ALL","/template/")
    for i in template[1]:
        print i 
        x = 1
        y = 1
        for n in i:
            y = 1
            for k in n:
                if k:
                    r = 15 
                    
                    place = (x * r + X - 5 - r/2 - len(i)  , y * r + Y - 5 - r/2 - len(i) , x * r + X - 5 + r/2 - len(i) , y * r + Y - 5 + r/2 - len(i) ) 
                    canvas.create_rectangle(place,fill="black",width = 0)
                    
                y += 1
            x += 1
        template[0].append(button(canvas,(X + 40,Y + 70),"Select","black"))
        Y += 150
    
    
    

def read_mat():
    global Mat,Map,Cell
    x = 0
    while x < len(Mat):
        y = 0
        while y < len(Mat[x]):
            if Mat[x][y] == 1:
                
                Cell.append(cell(canvas,Map.get_ratio(),(x,y)))

            y += 1
        x += 1

def read_cell():
    global Map,Mat,Cell
    x = 0
    while x < len(Cell):
        pos = Cell[x].get_pos()
        Mat[pos[0]][pos[1]] = 1
        Cell[x].sup(canvas)
        x += 1
    Cell = []
        
def check_cell():
    global Map,Mat,Cell
    M = copy.deepcopy(Mat)
            
    x = 0
    while x < len(Mat):
        y = 0
        while y < len(Mat):
            m = make_live(x,y)
            if m == 3 or m == 2 and Mat[x][y] == 1:
                M[x][y] = 1
            else:
                M[x][y] = 0
            y += 1
        x += 1
    Mat = M
            
            
def make_live(x,y):
    global Map,Mat,Cell
    X = [x-1,x,x+1]
    Y = [y-1,y,y+1]
    k = 0
    while k < len(X):
        if X[k] < 0 or X[k] >= len(Mat) :
            X.pop(k)
            k -= 1
        k += 1
    k = 0
    while k < len(Y):
        if Y[k] < 0 or Y[k] >= len(Mat) :
            Y.pop(k)
            k -= 1
        k += 1   
    s = 0
    
    for i in X:
        for j in Y:
            if Mat[i][j] == 1 and (j != y or i != x):
                s += 1
    return s
def reset(event):
    global speed,a
    X = event.x
    Y = event.y 
    if Button.act(X,Y):
        canvas.delete(ALL)
        start(current_map) 
    x = 0
    
    if a != 0:
        use_template(X,Y,a) 
   
    a = 0
    for i in template[0]:
        if i.act(X,Y):
            a = template[1][x]
        x += 1

    
def use_template(X,Y,a):
    X = (X + Map.get_ratio()/2) / Map.get_ratio()
    Y = (Y + Map.get_ratio()/2) / Map.get_ratio()
    x = 0
    while x < len(Mat):
        y = 0
        while y < len(Mat[x]):
            if 0 <= x-X < len(a) and 0 <= y-Y < len(a[x-X]) and a[x-X][y-Y] == 1:
                Mat[x][y] = 1
                
                print "match"
            y += 1
        x += 1
    

    
def main():
    global Map,Mat,Cell,Button,speed,count
    speed = Cursor.act(canvas,X,Y)
    if speed != 80 and count > speed:
        count = 0
        read_cell()
        check_cell()
        read_mat()
    count += 1
    fen.after(1,main)
    

start(current_map)
canvas.bind("<B1-Motion>", click)
canvas.bind("<Button-1>", reset)
canvas.pack()
fen.mainloop() 