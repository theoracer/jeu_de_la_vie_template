class cell():
    
    def __init__(self,canvas,ratio,pos):
        self.pos = pos 
        r = int(ratio/2)
        X,Y = (pos[0]+1)*ratio,(pos[1]+1)*ratio
        place = (X - r , Y - r , X + r , Y + r ) 
        self.id = canvas.create_rectangle(place,fill="black",width = 0)
    def sup(self,canvas):
        canvas.delete(self.id)
        
    def __del__(self):
        pass
        
    def get_pos(self):
        return self.pos